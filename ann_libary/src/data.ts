import { Drawable } from "./gfx";



export class Point implements Drawable{
    x: number;
    y: number;
    label: number;
    guessed: boolean;

    constructor(x: number, y: number, lable : number){
        this.x = x;
        this.y = y;
        this.label = lable;
        this.guessed = false;
        
    }
    draw(ctx: CanvasRenderingContext2D, cw: number, ch: number): void{
        ctx.fillStyle = "black";

        const drawX = this.x*cw;
        const drawY = this.y*ch;
        

        ctx.beginPath();
        ctx.arc(drawX, drawY, 10, 0, 2*Math.PI);
        if(this.label == 1){
            ctx.fill();
        }else{
            ctx.stroke();
        }
        ctx.beginPath();
        if(this.guessed){
            ctx.fillStyle = "green";
        }else{
            ctx.fillStyle = "red";
        }
        ctx.arc(drawX, drawY, 5, 0, 2*Math.PI);
        ctx.fill();
    }
}



export function generatePoints(count : number) : Point[]{
    const out : Point[] = [];
    for(let i = 0; i < count; i++){
        const x = Math.random();
        const y = Math.random();
        let label = 1;
        if(x < y){
            label = -1;
        }
        out.push(new Point(x, y, label));
        
    }

    return out;
}